# Glorify our Lord

## Contact 
For any questions contact me at `glorify.our.lord.webapp@gmail.com`.

## Development

### Configure Glorify our Lord
* Create a file at `docker/development/.env` and set the [required environment variables](#environment-variables)
  (using [this](https://docs.docker.com/compose/env-file/) syntax).
  Changes only apply after Glorify our Lord is restarted.

### Running Glorify our Lord
You will need [Docker](https://docs.docker.com/get-docker/) and 
[Docker Compose](https://docs.docker.com/compose/install/) installed.

#### Start Glorify our Lord
* Make sure a valid 
  [configuration](#configure-glorify-our-lord) of Glorify our Lord is defined
* From the directory `docker/development` run
  ````shell
  docker compose up
  ````
* You can access Glorify our Lord at `http://localhost:8080`.
  Hot-Reloading is enabled.
  This means that saving any changes to the frontend
  will immediately be applied to the Glorify our Lord instance started above.
* You can use PgAdmin4 to work with your local development database.
  PgAdmin4 is available at `http://localhost:8081`.
  For details see [here](#pgadmin).
* You can access the api provided by the backend at `http://localhost:9000`

#### Stop Glorify our Lord
From the directory `docker/development` run
````shell
docker compose stop
````

### PgAdmin

#### Log into PgAdmin
* Make sure Glorify our Lord is [running](#start-glorify-our-lord).
* In a browser open `http://localhost:8081`.
* For the login use the credentials
  * `Username`: pg-admin-user@test.com
  * `Password`: the password stored in `PG_ADMIN_PASSWORD` in the 
    [configuration](#configure-glorify-our-lord)

#### Configure PgAdmin
* [Log into PgAdmin](#log-into-pgadmin)
* Add a server for your local development database of Glorify our Lord
  * Right-Click on `Servers` and select `Create > Server..`
  * Choose a name in `General > Name`
  * Set `Connection > Host name/address` to `database`
  * Set `Connection > Port` to `5432`
  * Set `Connection > Username` to `database-user`
  * Set `Password` to the password stored in `DATABASE_PASSWORD` in the 
    [configuration](#configure-glorify-our-lord)

### Debugging

#### Debug the frontend
This section describes how to debug the fronted of Glorify our Lord using 
[Visual Studio Code](https://code.visualstudio.com/download). 
* Make sure you have Visual Studio Code, the browser of your choice
  and the corresponding Debugger extension installed 
  as described 
  [here](https://v2.vuejs.org/v2/cookbook/debugging-in-vscode.html#Prerequisites).
* Make sure Glorify our Lord is [running](#start-glorify-our-lord)
* In Visual Studio Code open the `client` folder of this project.
* Click on the Debugging icon in the Activity Bar to bring up the Debug view. 
* Click on the gear icon to configure a launch.json file.
* The json file below is an example for a configuration
  with which one can debug the frontend of Glorify our Lord in 
  Firefox, Chrome and Edge.
  ````json
  {
    "version": "0.2.0",
    "configurations": [
      {
        "type": "msedge",
        "request": "launch",
        "name": "Launch Edge against localhost",
        "url": "http://localhost:8080",
        "webRoot": "${workspaceFolder}"
      },
      {
        "type": "chrome",
        "request": "launch",
        "name": "Launch Chrome against localhost",
        "url": "http://localhost:8080",
        "webRoot": "${workspaceFolder}"
      },
      {
        "type": "firefox",
        "request": "launch",
        "name": "Launch Firefox against localhost",
        "url": "http://localhost:8080",
        "webRoot": "${workspaceFolder}",
        "pathMappings": [
          {
            "url": "webpack://client/src",
            "path": "${workspaceFolder}/src"
          }
        ]
      }
    ]
  }
  ````
* Set a breakpoint to a line by clicking to the left of the corresponding line number which causes a red dot to appear.
* Go to the Debug view, select a configuration, then press F5 or click the green play button.

#### Debug the backend
This section describes how to debug the fronted of Glorify our Lord using 
[Intellij](https://www.jetbrains.com/help/idea/installation-guide.html#standalone).
* In Intellij open the root folder of this project.
* Set up the debugger as explained 
  [here](https://www.jetbrains.com/help/idea/tutorial-remote-debug.html#debugger_rc)
  * `HOST` must be `localhost`
  * `PORT` must be `5005`
  * Only set up the debugger.
    You don't need to set up / start a host app yet.
    Especially, you can ignore the `Command line arguments for remote JVM`.
* Make sure Glorify our Lord is [configured](#configure-glorify-our-lord) with 
  `IS_SERVER_DEBUGGING_ENABLED` set to `true`.
* Make sure Glorify our Lord is [running](#start-glorify-our-lord).
  (Restart it, if you changed the configuration).
* As soon as the backend logs 
  ````
  Listening for transport dt_socket at address: 5005
  ````
  start the debugger configured in the beginning of this section
  with Alt+Shift+F9.
* Set a breakpoint to a line by clicking
  to the left of the corresponding line number which causes a red dot to appear.

## Environment variables
The environment variables described in this section
are part of the configuration of Glorify our Lord.

In the [configuration of a local instance](#configure-glorify-our-lord)
all variables with a _fixed value in development_ can not be changed
and hence can be ignored. 

| variable name               | description                                                                                                      | Value in development                                           | Value in production                                                                      |
|-----------------------------|------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------|------------------------------------------------------------------------------------------|
| DATABASE_PASSWORD           | Password for accessing the database                                                                              | A password of your choice                                      | See LastPass entry `Productive database variables`                                       |
| PG_ADMIN_PASSWORD           | Password for accessing PgAdmin                                                                                   | A password of your choice                                      | This variable is not needed in production                                                |
| IS_SERVER_DEBUGGING_ENABLED | Configures whether debugging for the backend is enabled                                                          | `true` if you want to debug the backend, `false` otherwise     | This variable is not needed in production                                                |
| JDBC_URL                    | Url at which the database is available                                                                           | Fixed value `jdbc:postgresql://database:5432/glorify-our-lord` | See LastPass entry `Productive database variables`                                       |
| DATABASE_USER               | User for accessing the database                                                                                  | Fixed value `database-user`                                    | See LastPass entry `Productive database variables`                                       |
| PORT                        | Port under which the backend will be accessible                                                                  | Fixed value `9000`                                             | Fixed value `8080`                                                                       |
| ALLOWED_ORIGINS             | Comma separated list with patterns. These patterns define those urls which can access api of the backend (CORS). | Fixed value `*`                                                | Fixed value `https://glorify-our-lord.firebaseapp.com, https://glorify-our-lord.web.app` |

## Productive Glorify our Lord
The productive instance of Glorify our Lord is available at
* https://glorify-our-lord.firebaseapp.com
* https://glorify-our-lord.web.app

### Productive Database
The productive instance of Glorify our Lord uses a database server
hosted by [ElephantSQL].
The credentials of the corresponding ElephantSQL account can be found
in the LastPass entry `elephantsql.com`.
The used ElephantSQL instance is called `Glorify-our-Lord`.

### Backend Host
The backend of the productive instance of Glorify our Lord
is deployed as a [back4app] project
called `glorify-our-lord-server`.
The back4app project is tied to the GitHub repository 
https://github.com/Tz3d4k4h/glorify-our-lord-server.
The credentials of the used back4app account
can be found in the LastPass entry `back4app.com`
and the credentials of the used GitHub account
can be found in the LastPass entry `github.com`.

### Frontend Host
The frontend of the productive instance of Glorify our Lord
is deployed to [Firebase].
The credentials of the corresponding Google account can be found
in the LastPass entry `Google Main`.
The used Firebase instance is called `Glorify our Lord`.

### Deployment Process
New versions of Glorify our Lord are deployed using [GitLab CI/CD].
Every push to the `master` branch triggers the deployment of a new version.
For details checkout the file `.gitlab-ci.yml`

[ElephantSQL]: https://www.elephantsql.com/
[back4app]: https://www.back4app.com/container-as-a-service-caas
[Firebase]: https://firebase.google.com/
[GitLab CI/CD]: https://docs.gitlab.com/ee/ci/
[GitHub]: https://github.com/