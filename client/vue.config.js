const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: [
    'quasar'
  ],

  pluginOptions: {
    quasar: {
      importStrategy: 'kebab',
      rtlSupport: false
    }
  },

  configureWebpack: config => {
    config.watchOptions = {
      aggregateTimeout: 200,
      poll: 1000,
    },
    config.devtool='source-map'
  }
})
