import QuoteData from './QuoteData'

/**
 * Model of a quote
 */
export default class Quote extends QuoteData {

  /**
   * ID of the quote
   * 
   * @type {number}
   */
  id;

  /**
   * Instantiates a Quote with the given content, author and id
   * 
   * @param {string} content of the instantiated Quote
   * @param {string} author  of the instantiated Quote
   * @param {number} id      of the instantiated Quote
   */
  constructor(content, author, id) {
    super(content, author)
    this.id = id;
  }

  /**
   * Creates a Quote with the data found in dataObject.
   * For every property in dataObject whose name matches the name a field of 
   * the Quote class the value of that property is used as value for that 
   * field.
   * 
   * @param {object} dataObject 
   * @returns Quote with data provided by dataObject or null / undefined if 
   * object is null / undefined
   */
  static createFromDataObject(dataObject) {
    if (dataObject == null) {
      return dataObject
    }

    return new Quote(
      dataObject.content,
      dataObject.author,
      dataObject.id
    )
  }
}