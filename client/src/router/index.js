import { createRouter, createWebHistory } from 'vue-router'
import Quotes from "@/components/Quotes";
import AddQuote from "@/components/AddQuote";
import EditQuote from "@/components/EditQuote";

const routes = [
  {
    path: '/quotes',
    name: 'List Quotes',
    component: Quotes,
  },
  {
    path: '/quotes/add',
    name: 'Add Quote',
    component: AddQuote
  },
  {
    path: '/quotes/:id/edit',
    name: 'Edit Quote',
    component: EditQuote,
  },
  {
    path: '/',
    redirect: '/quotes',
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
})

export default router