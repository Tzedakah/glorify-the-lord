
import './styles/quasar.sass'
import '@quasar/extras/material-icons/material-icons.css'
import {Notify} from 'quasar'
import { Meta } from 'quasar'

// To be used on app.use(Quasar, { ... })
export default {
  config: {},
  plugins: {
    Notify,
    Meta
  },
  extras: [
    'material-icons',
  ]
}