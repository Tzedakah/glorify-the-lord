import axios from "axios";
import {ensureNotificationIsShown, ensureNotificationIsNotShown} from "../notification/WaitingNotificationService";

/**
 * A ProgressTracker can be used to track whether a process is completed.
 * That process is called associated process.
 */
class ProgressTracker {

  /**
   * Instantiates a ProgressTracker which states that the associated process
   * is not completed.
   */
  constructor() {
    this.isCompleted = false;
  }

  /**
   * Executes the runnable if the associated process is not completed
   * 
   * @param {Function} runnable Function with no parameters
   */
  ifNotCompleted(runnable) {
    if (!this.isCompleted) {
      runnable();
    }
  }

  /**
   * Reports that the associated process is completed.
   */
  reportCompletion() {
    this.isCompleted = true;
  }
}

/**
 * Executes sendAxiosRequest.
 * (A promise on) the AxiosResponse returned by sendAxiosRequest is returned.
 * While waiting for the execution of sendAxiosRequest to finish a toast is 
 * displayed that asks the user to wait until a connection to the server is 
 * established.
 * To be precise, the toast is shown from 300 milliseconds after 
 * sendAxiosRequest is executed until that execution is finished.
 * For that, it does not matter if the execution finishes with an AxiosResponse
 * or an error.
 * 
 * @param {Function} sendAxiosRequest Function with contract: 
 *                                    () => AxiosResponse<any, any>
 * 
 * @returns {Promise<AxiosResponse<any, any>>} (A promise on) the AxiosResponse 
 * returned by sendAxiosRequest is returned.
 */
async function performWithWaitingNotification(sendAxiosRequest) {
  let requestProgressTracker = new ProgressTracker()
  setTimeout(
    () => requestProgressTracker.ifNotCompleted(ensureNotificationIsShown),
    300
  )
  try {
    return await sendAxiosRequest(); 
  } finally {
    requestProgressTracker.reportCompletion();
    ensureNotificationIsNotShown();
  }
}

/**
 * A PatientHttpClient is an HTTP client.
 * Requests sent with a PatientHttpClient by default are sent to urls relative 
 * to fixed base url, which is called associated to that PatientHttpClient.
 * Requests sent with a PatientHttpClient have a long timeout time.
 * Shortly after a request is sent with a PatientHttpClient, a toast is shown
 * unless the execution of the request already finished.
 * It will be shown until the execution of the request is finished.
 * For that, it does not matter if that execution finishes with a received 
 * reponse or an error.
 */
export default class PatientHttpClient {

  /**
   * This AxiosInstance is used for sending HTTP requests. 
   */
  instance;


  /**
   * @param {string} baseURL associated base url of the instantiated 
   *                         PatientHttpClient
   */
  constructor(baseUrl) {
    this.instance = axios.create(
      {
        baseURL: baseUrl,
        timeout: 180000,
      } 
    );
  }

  /**
   * Sends a GET request to the given relative url.
   * By default the associated base url of this PatientHttpClient is used as 
   * base url for the GET request.
   * A toast is shown when the execution of the request takes longer. 
   * For details see the documentation of this class.
   * 
   * @param {string} relativeUrl             relative url of the sent GET 
   *                                         request
   * @param {AxiosRequestConfig<any>} config configuration of the sent GET
   *                                         request
   * @returns (A promise on) a response of the sent GET request.  
   */
  get(relativeUrl, config) {
    return performWithWaitingNotification(
      () => this.instance.get(relativeUrl, config)
    );
  }

  /**
   * Sends a PUT request to the given relative url with the given data as 
   * payload.
   * By default the associated base url of this PatientHttpClient is used as 
   * base url for the PUT request.
   * A toast is shown when the execution of the request takes longer. 
   * For details see the documentation of this class.
   * 
   * @param {string} relativeUrl             relative url of the sent PUT 
   *                                         request
   * @param {*} data                         payload of the sent PUT 
   *                                         request
   * @param {AxiosRequestConfig<any>} config configuration of the sent PUT
   *                                         request
   * @returns (A promise on) a response of the sent PUT request.  
   */
  put(relativeUrl, data, config) {
    return performWithWaitingNotification(
      () => this.instance.put(relativeUrl, data, config)
    )
  }

  /**
   * Sends a POST request to the given relative url with the given data as 
   * payload.
   * By default the associated base url of this PatientHttpClient is used as 
   * base url for the POST request.
   * A toast is shown when the execution of the request takes longer. 
   * For details see the documentation of this class.
   * 
   * @param {string} relativeUrl             relative url of the sent POST 
   *                                         request
   * @param {*} data                         payload of the sent POST 
   *                                         request
   * @param {AxiosRequestConfig<any>} config configuration of the sent POST
   *                                         request
   * @returns (A promise on) a response of the sent POST request.  
   */
  post(relativeUrl, data, config) {
    return performWithWaitingNotification(
      () => this.instance.post(relativeUrl, data, config)
    )
  } 

  /**
   * Sends a DELETE request to the given relative url.
   * By default the associated base url of this PatientHttpClient is used as 
   * base url for the DELETE request.
   * A toast is shown when the execution of the request takes longer. 
   * For details see the documentation of this class.
   * 
   * @param {string} relativeUrl             relative url of the sent DELETE
   *                                         request
   * @param {AxiosRequestConfig<any>} config configuration of the sent DELETE
   *                                         request
   * @returns (A promise on) a response of the sent DELETE request.  
   */
  delete(relativeUrl, config) {
    return performWithWaitingNotification(
      () => this.instance.delete(relativeUrl, config)
    )
  }
}