import PatientHttpClient from './PatientHttpClient'

/**
 * An http client which by defaults sends requests against the quote
 * part of the api of the server.
 */
const patientHttpClient = new PatientHttpClient(
  process.env.VUE_APP_SERVER_URI
    ? process.env.VUE_APP_SERVER_URI + '/api/1.1/quote/'
    : 'api/1.0/quote/'
)

const createApi = () => {
  return {

    // (C)reate
    createNew(quoteData) {
      return patientHttpClient.post(
        'new',
        quoteData,
        {
          headers: {
            'Content-Type': 'application/json'
          }
        }
      )
    },

    // (R)ead
    getAll() {
      return patientHttpClient.get('all')
    },

    // (R)ead
    getForId(id) {
      return patientHttpClient.get(
        String(id)
      )
    },

    // (U)pdate
    updateForId(id, quoteData) {
      return patientHttpClient.post(
        'update/' + id,
        quoteData,
        {
          headers: {
            'Content-Type': 'application/json'
          }
        },
      )
    },

    // (D)elete
    removeForId(id) {
      return patientHttpClient.delete(
        String(id)
      )
    }
  }
}

export default createApi