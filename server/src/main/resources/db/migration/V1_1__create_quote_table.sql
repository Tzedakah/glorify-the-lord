CREATE TABLE public.quote (
    id bigint NOT NULL PRIMARY KEY,
    content text,
    author text,
    source text,
    version bigint NOT NULL
);