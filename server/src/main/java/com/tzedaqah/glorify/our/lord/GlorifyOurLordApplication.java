package com.tzedaqah.glorify.our.lord;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GlorifyOurLordApplication {

	public static void main(String[] args) {
		SpringApplication.run(GlorifyOurLordApplication.class, args);
	}

}
