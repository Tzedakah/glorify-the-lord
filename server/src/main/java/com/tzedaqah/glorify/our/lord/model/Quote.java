package com.tzedaqah.glorify.our.lord.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Version;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * A <code>Quote</code> represents a quote form a person.
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Quote {

    /**
     * Id
     */
    @Id
    @GeneratedValue
    private Long id;

    /**
     * Version used for optimistic locking.
     */
    @Version
    private Integer version;

    /**
     * The content of the quote is what the quote says.
     */
    private String content;

    /**
     * The author of the quote is who said the quote.
     */
    private String author;

    /**
     * The source of the quote says where to find the quote, e.g. a link.
     */
    private String source;

}
