package com.tzedaqah.glorify.our.lord.repository;

import com.tzedaqah.glorify.our.lord.model.Quote;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * The <code>QuoteRepository</code> allows to perform CRUD operations on the entity <code>Quote</code>.
 */
@Repository
public interface QuoteRepository extends CrudRepository<Quote, Long> {
}
