package com.tzedaqah.glorify.our.lord.service;

import com.tzedaqah.glorify.our.lord.model.Quote;
import com.tzedaqah.glorify.our.lord.repository.QuoteRepository;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * This service allows to perform tasks with {@link Quote Quotes}.
 */
@Service
@AllArgsConstructor
public class QuoteService {

    /**
     * This <code>QuoteRepository</code> allows to interact with the {@link Quote Quotes} in the database.
     */
    @Autowired
    private final QuoteRepository repository;

    /**
     * @return <code>Quote</code>s currently stored in the database
     */
    @NonNull
    public List<Quote> getAll() {
        return StreamSupport
                .stream(
                        repository.findAll().spliterator(),
                        false
                )
                .collect(Collectors.toList());
    }

    /**
     * @return <code>Quote</code> whose {@link Quote#getId() id} is <code>quoteId</code> or null if no such
     * <code>Quote</code> exists
     */
    public Quote getById(long quoteId) {
        return repository.findById(quoteId).orElse(null);
    }

    /**
     * Creates a new <code>Quote</code> with 
     * <ul>
     *     <li>
     *         the {@link Quote#getContent() content} given by <code>contentOfNewQuote</code>.
     *     </li>
     *     <li>
     *         the {@link Quote#getAuthor() author} given by <code>authorOfNewQuote</code>.
     *     </li>
     * </ul>
     * This <code>Quote</code> is stored in the database.
     *
     * @param contentOfNewQuote content of the new <code>Quote</code>
     * @param authorOfNewQuote author of the new <code>Quote</code>
     * @return the newly created <code>Quote</code>
     */
    public Quote addQuoteWithContentAndAuthor(
            String contentOfNewQuote,
            String authorOfNewQuote

    ) {
        var newQuote = Quote.builder()
                .content(contentOfNewQuote)
                .author(authorOfNewQuote)
                .build();
        return repository.save(newQuote);
    }

    /**
     * Deletes the <code>Quote</code> with the {@link Quote#getId() id} given by <code>quoteId</code> from the database.
     *
     * @param quoteId non-null {@link Quote#getId() id} of the <code>Quote</code> to be deleted
     */
    public void remove(long quoteId) {
        repository.deleteById(quoteId);
    }

    /**
     * This method updates the <code>Quote</code> whose {@link Quote#getId() id} is <code>quoteId</code>.
     * <ul>
     *     <li>
     *         The {@link Quote#getContent() content} of that <code>Quote</code> is set to
     *         <code>newContentOfQuote</code>.
     *     </li>
     *     <li>
     *         The {@link Quote#getAuthor() author} of that <code>Quote</code> is set to <code>newAuthorOfQuote</code>.
     *     </li>
     * </ul>
     * This change is persisted in the database.
     * Expects <code>quoteId</code> to be the {@link Quote#getId() id} of an existing <code>Quote</code> in the
     * database.
     *
     * @param quoteId           {@link Quote#getId() id} of a <code>Quote</code> that exists in the database and will
     *                          be updated
     * @param newContentOfQuote new {@link Quote#getContent() content} of the updated <code>Quote</code>
     * @param newAuthorOfQuote  new {@link Quote#getAuthor() author} of the updated <code>Quote</code>
     * @return the updated <code>Quote</code>
     * @throws IllegalArgumentException if no <code>Quote</code> with the {@link Quote#getId() id} <code>quoteId</code>
     *                                  exists in the database
     */
    public Quote updateContentAndAuthorOfQuote(
            long quoteId,
            String newContentOfQuote,
            String newAuthorOfQuote
    ) {
        var quoteOptional = repository.findById(quoteId);

        if (quoteOptional.isEmpty()) {
            throw new IllegalArgumentException("There is no quote with the given id.");
        }
        var quote = quoteOptional.get();

        quote.setContent(newContentOfQuote);
        quote.setAuthor(newAuthorOfQuote);
        repository.save(quote);

        return quote;
    }
}
